// adapterGenerator(anyArguments...) => adapterConstructor.call(adapter)
module.exports = function adapterGenerator(collection) {
  return function adapterConstructor() {
    
    // (id: String, callback: (error?, result?: any))
    this.getRef = function(id, callback) {
      callback(undefined, collection.get(parseInt(id)));
    };
    
    // (data: any) => id: String
    this.newRef = function(data) {
      return data.$loki;
    };
    
    // (id: String, callback: (error?, result?: Boolean))
    this.deleteRef = function(id, callback) {
      this.getRef(id, function(error, data) {
        if (data) {
          collection.remove(data);
          callback(undefined, true);
        } else {
          callback(undefined, false);
        }
      });
    };
    
    return this;
    
  };
};