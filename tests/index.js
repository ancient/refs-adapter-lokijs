var Loki = require('lokijs');
var Refs = require('ancient-refs');
var DocumentsFactory = require('ancient-document').Factory;
var Adapters = require('ancient-adapters').Adapters;
var AncientRefsAdapterLokiJS = require('../index.js');
var TestAncientRefs = require('ancient-refs/tests/test.js');

describe('ancient-cursor-adapter-lokijs', function() {
  var db = new Loki(this.title);
  var collection = db.addCollection(this.title);
  collection.insert({});
  collection.insert({});
  collection.insert({});
  
  var results = collection.find();
  var requiredResults = {};
  
  for (var r in results) {
    requiredResults['test' + '/' + results[r].$loki] = results[r];
  }
  
  var adapters = new Adapters();
  adapters.add('test', AncientRefsAdapterLokiJS(collection));
  
  var documentsFactory = new DocumentsFactory(adapters);
  
  TestAncientRefs(Refs, documentsFactory, 'test', requiredResults, true, true);
});