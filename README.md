# RefsAdapterLokiJS^0.0.0 ^[wiki](https://gitlab.com/ancient/refs-adapter-lokijs/wikis/home)

Adapter based on [LokiJS](http://lokijs.org/#/) for [ancient-refs](https://gitlab.com/ancient/refs) package.

## Example

```js
var loki = require('lokijs');

var Adapters = require('ancient-adapters').Adapters;
var Refs = require('ancient-refs');
var DocumentsFactory = require('ancient-document').Factory;
var AncientRefsAdapterLokiJS = require('ancient-refs-adapter-lokijs');

var db = new loki('db');
var posts = db.addCollection('posts');

posts.insert({ body: 'Hello World!' });
// { $loki: 1, body: 'Hello World!' }

var adapters = new Adapters();
adapters.add('posts', AncientRefsAdapterLokiJS(posts));
// or equal
// adapters.add('posts', AncientRefsAdapterLokiJS(posts).call({}));

var documentsFactory = new DocumentsFactory(adapters);

var refs = new Refs(documentsFactory);
refs.get('posts/1'); // Document { data: { $loki: 1, body: 'Hello World!' } }
refs.new('posts', { $loki: 1, body: 'Hello World!' }); // 'posts/1';
refs.delete('posts/1'); // true
refs.get('posts/1'); // undefined;
```